package com.s2s.dbconnection;

import java.sql.Connection;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * Class for working with DB instance
 */
public class DatabaseAccess {

	private static DatabaseAccess singletonDatabaseAccess = null;
	private DatabaseConnection databaseConnector;
	private DatabaseQueries databaseQueries;
	private Connection connection;

	/**
	 * Getting instance of DB access object
	 * @return {DatabaseAccess}
	 */
	public static DatabaseAccess getInstance() {
		if (singletonDatabaseAccess == null) {
			singletonDatabaseAccess = new DatabaseAccess();
		}
		return singletonDatabaseAccess;
	};

	/**
	 * Constructor
	 */
	private DatabaseAccess() {
		this.databaseConnector = new MySqlConnector();
		this.connection = databaseConnector.getConnection();
		this.databaseQueries = new MySqlDatabaseQueries(this.connection);
	}

	/**
	 * Cheking is user have access to application according on User table in DB
	 * @param {String} username Cheking username
	 * @param {String} password Cheking password
	 * @return {Boolean} True: username and password are correct => user have an access;
	 *                   False: smth wrong with credentials: user haven't access
	 */
	public boolean checkUserAccess(String username, String password) {
		int existingUsers = this.databaseQueries.checkUser(username, password);
		if (existingUsers > 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * Extract data from joined tables: Deals, Instruments, Counterparty
	 * @param {long} deal_id Filter parameter, equal -1 when we don't want filter
	 * @param {String} from_deal_time Filter parameter, equal "" when we don't want filter
	 * @param {String} to_deal_time Filter parameter, equal "" when we don't want filter
	 * @param {String} deal_type Filter parameter, equal "" when we don't want filter
	 * @param {double} from_deal_value Filter parameter, equal -1 when we don't want filter
	 * @param {double} to_deal_value Filter parameter, equal -1 when we don't want filter
	 * @param {String} instrument_name Filter parameter, equal "" when we don't want filter
	 * @param {String} counterparty_name Filter parameter, equal "" when we don't want filter
	 * @param {String} counterparty_status Filter parameter, equal "" when we don't want filter
	 * @return {ArrayList} Array of Deal objects with DB values
	 * @throws ParseException
	 */
	public ArrayList<Deal> getDeals(long deal_id, String from_deal_time, String to_deal_time, String deal_type,
			double from_deal_value, double to_deal_value, String instrument_name, String counterparty_name,
			String counterparty_status) throws ParseException{
		
		return this.databaseQueries.extractDeals(deal_id, from_deal_time, to_deal_time, deal_type,
				from_deal_value, to_deal_value, instrument_name, counterparty_name,
				counterparty_status);
	}
	
	public ArrayList<AverageBuyAndSell> getAveragesValues() {
		return this.databaseQueries.getAverage();
	}
	
	public ArrayList<EndingPositions> getEndPositions() {
		return this.databaseQueries.getEndingPositions();
	}
	
	public ArrayList<NetValues> getNetValues() {
		return this.databaseQueries.getNetSum();
	}
	
	public ArrayList<NetValues> getEffectiveSum() {
		return this.databaseQueries.getEffectiveSum();
	}
}
