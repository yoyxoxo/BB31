package com.s2s.dbconnection;

public class EndingPositions {
	private String counterparty;
	private String instrument;
	private int end_positions;
	
	public EndingPositions() {
	}
	
	public String getCounterparty() {
		return counterparty;
	}
	
	public void setCounterparty(String counterparty) {
		this.counterparty = counterparty;
	}
	
	public String getInstrument() {
		return instrument;
	}
	
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	
	public int getEnd_positions() {
		return end_positions;
	}
	
	public void setEnd_positions(int end_positions) {
		this.end_positions = end_positions;
	}
}
