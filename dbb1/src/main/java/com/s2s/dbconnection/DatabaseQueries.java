package com.s2s.dbconnection;

import java.util.ArrayList;

public interface DatabaseQueries {
	int checkUser(String username, String password);
	public ArrayList<Deal> extractDeals(long deal_id, String from_deal_time, String to_deal_time, String deal_type,
			double from_deal_value, double to_deal_value, String instrument_name, String counterparty_name,
			String counterparty_status);
	public ArrayList<AverageBuyAndSell> getAverage();
	public ArrayList<EndingPositions> getEndingPositions();
	public ArrayList<NetValues> getNetSum();
	public ArrayList<NetValues> getEffectiveSum();
}
