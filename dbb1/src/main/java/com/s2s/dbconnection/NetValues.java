package com.s2s.dbconnection;

public class NetValues {
	private String counterparty;
	private int net;
	
	public NetValues() {
	}
	
	public String getCounterparty() {
		return counterparty;
	}
	
	public void setCounterparty(String counterparty) {
		this.counterparty = counterparty;
	}
	
	public int getNet() {
		return net;
	}
	
	public void setNet(int net) {
		this.net = net;
	}
}