package com.s2s.dbconnection;

import java.sql.Connection;

/**
 * DatabaseConnection interface
 */
public interface DatabaseConnection {

	Connection connection = null;
	Connection getConnection();
	
	void startConnection();
	void closeConnection();
}