package com.s2s.dbconnection;

public class AverageBuyAndSell {
	private String instrument_name;
	private int average_buy_price;
	private int average_sell_price;
	
	public AverageBuyAndSell() {
	}

	public String getInstrument_name() {
		return instrument_name;
	}

	public void setInstrument_name(String instrument_name) {
		this.instrument_name = instrument_name;
	}

	public int getAverage_buy_price() {
		return average_buy_price;
	}

	public void setAverage_buy_price(int average_buy_price) {
		this.average_buy_price = average_buy_price;
	}

	public int getAverage_sell_price() {
		return average_sell_price;
	}

	public void setAverage_sell_price(int average_sell_price) {
		this.average_sell_price = average_sell_price;
	}
}