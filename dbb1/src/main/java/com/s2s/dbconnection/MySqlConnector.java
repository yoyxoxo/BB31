package com.s2s.dbconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Class that represents DatabaseConnection interface
 */
public class MySqlConnector implements DatabaseConnection {

	private Connection connection;

	/**
	 * DB connector
	 */
	protected MySqlConnector() {
		startConnection();
	}

	/**
	 * Start connection with DB
	 */
	@Override
	public void startConnection(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://34.250.108.7/db_grad_cs_1917?useSSL=false",
					"test-user", "test-user");
			
			//connection = DriverManager.getConnection("jdbc:mysql://192.168.99.100/db_grad_cs_1917?useSSL=false",
			//		"test-user", "test-user");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			// Smth wrong with Sql execution
			e.printStackTrace();
		}

	}

	/**
	 * Close connection
	 */
	public void closeConnection() {
	
		if (connection != null) {
			try {
				connection.close();
				connection = null;
			} catch (SQLException e) {

			}
		}
	}

	/**
	 * Get connection instance
	 */
	public Connection getConnection() {
		return connection;
	}

}