package com.s2s.dbconnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * Class that represents DatabaseQueries interface
 */
public class MySqlDatabaseQueries implements DatabaseQueries {

	private Statement statement;

	/**
	 * Create DB statement
	 * @param {Connection} connection
	 */
	public MySqlDatabaseQueries(Connection connection) {
		try {
			this.statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Query to check that user with credentions exist in DB
	 * @param {String} username Cheking username
	 * @param {String} password Cheking password
	 * returns {int} rowCount Count of returned rows. If it = 0 => credentials wrong
	 */
	public int checkUser(String username, String password) {

		int rowCount = 0;
		try {
			ResultSet rs = statement.executeQuery("Select count(*) from users u where u.user_id = " + "\"" + username
					+ "\"" + " AND u.user_pwd = \"" + password + "\"");

			rs.next();
			rowCount = rs.getInt(1);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return rowCount;
	}

	/**
	 * Query to exrtact data from joined tables: Deals, Instruments, Counterparty
	 * @param {long} deal_id Filter parameter, equal -1 when we don't want filter
	 * @param {String} from_deal_time Filter parameter, equal "" when we don't want filter
	 * @param {String} to_deal_time Filter parameter, equal "" when we don't want filter
	 * @param {String} deal_type Filter parameter, equal "" when we don't want filter
	 * @param {double} from_deal_value Filter parameter, equal -1 when we don't want filter
	 * @param {double} to_deal_value Filter parameter, equal -1 when we don't want filter
	 * @param {String} instrument_name Filter parameter, equal "" when we don't want filter
	 * @param {String} counterparty_name Filter parameter, equal "" when we don't want filter
	 * @param {String} counterparty_status Filter parameter, equal "" when we don't want filter
	 * @return {ArrayList} Array of Deal objects with DB values
	 */
	public ArrayList<Deal> extractDeals(long deal_id, String from_deal_time, String to_deal_time, String deal_type,
			double from_deal_value, double to_deal_value, String instrument_name, String counterparty_name,
			String counterparty_status) {
			
			String query = "Select deal_id"
					+ ", deal_time"
					+ ", deal_type "
					+ ", deal_amount , instrument_name "
					+", deal_quantity, counterparty_name "
					+", counterparty_status "
					+", counterparty_date_registered "
				+"from instrument as i "
				+"INNER JOIN deal  as d ON d.deal_instrument_id = i.instrument_id "
				+"INNER JOIN counterparty c ON d.deal_counterparty_id = c.counterparty_id "
				+ "WHERE true";
			
			if(deal_id != -1){
				query += " AND deal_id = " + deal_id;
			}
			
			if( from_deal_time != ""){		
				query += " AND REPLACE(deal_time, 'T', ' ') >= '" + from_deal_time + "'";
			}
			
			if( to_deal_time != ""){
				query += " AND REPLACE(deal_time, 'T', ' ') <= '" + to_deal_time + "'";
			}
			
			if( deal_type != ""){
				query += " AND deal_type = '" + deal_type + "'";
			}
			
			if(from_deal_value != -1){
				query += " AND deal_amount*deal_quantity >= " + from_deal_value;
			}
			
			if(to_deal_value != -1){
				query += " AND deal_amount*deal_quantity <= " + to_deal_value;
			}
			
			if( instrument_name != ""){
				query += " AND instrument_name = '" + instrument_name + "'";
			}
			
			if( counterparty_name != ""){
				query += " AND counterparty_name = '" + counterparty_name + "'";
			}
			
			if(counterparty_status != ""){
				query += " AND counterparty_status = '" + counterparty_status + "'";
			}
			
			query += " LIMIT 50;";
		
			ArrayList<Deal> deals = new ArrayList<Deal>();

			try {
				ResultSet rs = statement.executeQuery(query);
				
				while(rs.next()) {
					Deal deal = new Deal();      
					deal.setDeal_id(rs.getInt("deal_id"));
					deal.setDeal_time(rs.getString("deal_time"));
					deal.setDeal_type(rs.getString("deal_type"));
					deal.setDeal_amount(rs.getDouble("deal_amount"));
					deal.setQuantity(rs.getInt("deal_quantity"));
					deal.setInstrument_name(rs.getString("instrument_name"));
					deal.setCounterparty_name(rs.getString("counterparty_name"));
					deal.setCounterparty_status(rs.getString("counterparty_status"));
					deal.setCounterparty_date_registered(rs.getString("counterparty_date_registered"));
			
				    deals.add(deal);
				} 
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return deals;
		}
	
	/**
	 * Average buy and sell prices for each instrument during the period
	 */
	public ArrayList<AverageBuyAndSell> getAverage(){
		String query = ""
				+" Select b.instrument_name , average_buy_price, average_sell_price from "
				+" ("
				+"   Select AVG(d.deal_amount) AS average_buy_price ,i.instrument_name, d.deal_type"
				+"   FROM deal AS d"
				+"   INNER JOIN instrument AS i"
				+"   ON d.deal_instrument_id = i.instrument_id"
				+"   GROUP BY i.instrument_name,d.deal_type"
				+"   HAVING d.deal_type = 'B'"
				+" ) As b"
				+" INNER JOIN"
				+"   (Select AVG(d.deal_amount) AS average_sell_price, i.instrument_name, d.deal_type"
				+"   FROM deal AS d"
				+"   INNER JOIN instrument AS i"
				+"   ON d.deal_instrument_id = i.instrument_id"
				+"   GROUP BY i.instrument_name,d.deal_type"
				+"   HAVING d.deal_type = 'S'"
				+" ) AS s"
				+" ON b.instrument_name = s.instrument_name;";		
				
		ArrayList<AverageBuyAndSell> averageValues = new ArrayList<AverageBuyAndSell>();

		try {
			ResultSet rs = statement.executeQuery(query);
			
			while(rs.next()) {
				AverageBuyAndSell averageValue = new AverageBuyAndSell();      
				averageValue.setInstrument_name(rs.getString("instrument_name"));
				averageValue.setAverage_buy_price(rs.getInt("average_buy_price"));
				averageValue.setAverage_sell_price(rs.getInt("average_sell_price"));
				
				averageValues.add(averageValue);
			} 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return averageValues;
	}
	
	public ArrayList<EndingPositions> getEndingPositions(){
		
		String query = ""
				+" SELECT c.counterparty_name AS counterparty, i.instrument_name AS instrument, "
				+" SUM( CASE WHEN deal_type = 'B' THEN d.deal_quantity ELSE -d.deal_quantity END) AS end_positions"
				+" FROM deal AS d"
				+" INNER JOIN counterparty AS c ON c.counterparty_id = d.deal_counterparty_id"
				+" INNER JOIN instrument AS i ON i.instrument_id = d.deal_instrument_id"
				+" GROUP BY counterparty_id, i.instrument_id, c.counterparty_id, i.instrument_name;";	
		
		ArrayList<EndingPositions> endingPositions = new ArrayList<EndingPositions>();

		try {
			ResultSet rs = statement.executeQuery(query);
			
			while(rs.next()) {
				EndingPositions endingPosition = new EndingPositions();  
				endingPosition.setCounterparty(rs.getString("counterparty"));
				endingPosition.setInstrument(rs.getString("instrument"));
				endingPosition.setEnd_positions(rs.getInt("end_positions"));
				
				endingPositions.add(endingPosition);
			} 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return endingPositions;
	}
	
public ArrayList<NetValues> getNetSum(){
		
		String query = ""
				+" Select counterparty_name AS counterparty, "
				+" Sum( case when deal_type = 'B' then -(d.deal_amount * d.deal_quantity) else (d.deal_amount * d.deal_quantity) end) as net"
				+" FROM deal AS d"
				+" INNER JOIN counterparty ON counterparty.counterparty_id = d.deal_counterparty_id"
				+" INNER JOIN instrument ON instrument.instrument_id = d.deal_instrument_id"
				+" GROUP BY counterparty_id;";		
				
		ArrayList<NetValues> netValues = new ArrayList<NetValues>();
		try {
			ResultSet rs = statement.executeQuery(query);
			
			while(rs.next()) {
				NetValues netValue = new NetValues();  
				netValue.setCounterparty(rs.getString("counterparty"));
				netValue.setNet(rs.getInt("net"));
				
				netValues.add(netValue);
			} 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return netValues;
	}

public ArrayList<NetValues> getEffectiveSum(){
	
	String query = ""
			+ "SELECT net_value, c.counterparty_name FROM ("
			+" SELECT trading_result.counterparty_id as counterparty_id, ( trades + stock_value) AS net_value FROM"
			+" (SELECT  counterparty_id, counterparty.counterparty_name as counterparty_name,"
			+" Sum( case when deal_type = 'B' then -(d.deal_amount * d.deal_quantity) else (d.deal_amount * d.deal_quantity) end) AS trades"
			+" FROM deal AS d"
			+" INNER JOIN counterparty ON counterparty.counterparty_id = d.deal_counterparty_id"
			+" INNER JOIN instrument ON instrument.instrument_id = d.deal_instrument_id"
			+" GROUP BY counterparty_id) trading_result"
			+" ,"
			+" (Select  counterparty_id ,SUM(ending_value_per_instrument) AS stock_value FROM ("
			+"  SELECT instruments_per_party.counterparty_id, instruments_per_party.instrument_id,"
			+" (instruments_per_party.final_quantity * ending_prices.end_price) AS ending_value_per_instrument"
			+" FROM "
			+" (SELECT  counterparty_id, i.instrument_id,"
			+" SUM( CASE WHEN deal_type = 'B' THEN d.deal_quantity ELSE -d.deal_quantity END) AS final_quantity"
			+" FROM deal AS d"
			+" INNER JOIN counterparty AS c ON c.counterparty_id = d.deal_counterparty_id"
			+" INNER JOIN instrument AS i ON i.instrument_id = d.deal_instrument_id"
			+" GROUP BY counterparty_id, i.instrument_id) AS instruments_per_party"
			+" INNER JOIN"
			+" (Select d.deal_instrument_id, AVG(d.deal_amount) AS end_price FROM deal as d,("
			+" SELECT d.deal_instrument_id, MAX(d.deal_time) last_deal_time FROM deal AS d"
			+" GROUP BY d.deal_instrument_id"
			+" )  AS pt"
			+" WHERE d.deal_time =  pt.last_deal_time AND pt.deal_instrument_id = d.deal_instrument_id"
			+" GROUP BY d.deal_instrument_id) AS ending_prices"
			+" ON instruments_per_party.instrument_id = ending_prices.deal_instrument_id) AS value_amount_instrument"
			+" Group by counterparty_id) AS stock_value"
			+" where trading_result.counterparty_id = stock_value.counterparty_id ) AS main_table"
			+" INNER JOIN counterparty AS c ON c.counterparty_id = main_table.counterparty_id;";		
			
			
	ArrayList<NetValues> netValues = new ArrayList<NetValues>();
	try {
		ResultSet rs = statement.executeQuery(query);
		
		while(rs.next()) {
			NetValues netValue = new NetValues();  
			netValue.setCounterparty(rs.getString("counterparty_name"));
			netValue.setNet(rs.getInt("net_value"));
			
			netValues.add(netValue);
		} 
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return netValues;
   }
}

