package com.s2s.dbconnection;

/**
 * Class that represents data from joined tables: Deals, Instruments, Counterparty
 */
public class Deal {

	private long deal_id;
	private String deal_time;
	private String deal_type;
	private double deal_amount;
	private int quantity;
	private String instrument_name;
	private String counterparty_name;
	private String counterparty_status;
	private String counterparty_date_registered;

	public Deal() {
	}

	public long getDeal_id() {
		return deal_id;
	}

	public void setDeal_id(long deal_id) {
		this.deal_id = deal_id;
	}

	public String getDeal_time() {
		return deal_time;
	}

	public void setDeal_time(String deal_time) {
		this.deal_time = deal_time;
	}

	public double getDeal_amount() {
		return deal_amount;
	}

	public void setDeal_amount(double deal_amount) {
		this.deal_amount = deal_amount;
	}

	public int getQuantity() {
		return quantity;
	}
	
	public String getDeal_type() {
		return deal_type;
	}

	public void setDeal_type(String deal_type) {
		this.deal_type = deal_type;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getInstrument_name() {
		return instrument_name;
	}

	public void setInstrument_name(String instrument_name) {
		this.instrument_name = instrument_name;
	}

	public String getCounterparty_name() {
		return counterparty_name;
	}

	public void setCounterparty_name(String counterparty_name) {
		this.counterparty_name = counterparty_name;
	}

	public String getCounterparty_status() {
		return counterparty_status;
	}

	public void setCounterparty_status(String counterparty_status) {
		this.counterparty_status = counterparty_status;
	}

	public String getCounterparty_date_registered() {
		return counterparty_date_registered;
	}

	public void setCounterparty_date_registered(String counterparty_date_registered) {
		this.counterparty_date_registered = counterparty_date_registered;
	}

}