package com.s2s.demomidtier;

import java.text.ParseException;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.s2s.dbconnection.AverageBuyAndSell;
import com.s2s.dbconnection.DatabaseAccess;
import com.s2s.dbconnection.Deal;
import com.s2s.dbconnection.EndingPositions;
import com.s2s.dbconnection.NetValues;

/**
 * Class that links backend and frontend
 */
public class Responder {

	private DatabaseAccess databaseAccess;
	private boolean isConnection = false;
	
	public Responder() {
		databaseAccess = DatabaseAccess.getInstance();
	}
	
	/**
	 * Method for getting data from joined tables: Deals, Instruments, Counterparty
	 * @return {ArrayList<Deal>}
	 * @throws ParseException
	 */
	public ArrayList<Deal> getDeals(long deal_id, String from_deal_time, String to_deal_time, String deal_type,
			double from_deal_value, double to_deal_value, String instrument_name, String counterparty_name,
			String counterparty_status) throws ParseException{
		return databaseAccess.getDeals(deal_id, from_deal_time, to_deal_time, deal_type,
				from_deal_value, to_deal_value, instrument_name, counterparty_name,
				counterparty_status);
	}
	
	public ArrayList<AverageBuyAndSell> getAverages() {
		return databaseAccess.getAveragesValues();
	}
	
	public ArrayList<EndingPositions> getEndPositions() {
		return databaseAccess.getEndPositions();
	}
	
	public ArrayList<NetValues> getNetValues() {
		return databaseAccess.getNetValues();
	}
	
	public ArrayList<NetValues> getEffectiveSum() {
		return databaseAccess.getEffectiveSum();
	}
	
	/**
	 * Cheking is user have access to application according on User table in DB
	 * @param {String} username Cheking username
	 * @param {String} password Cheking password
	 * @return {Boolean} True: username and password are correct => user have an access;
	 *                   False: smth wrong with credentials: user haven't access
	 */
	public boolean isUser(String username, String password){
		return this.databaseAccess.checkUserAccess(username, password);
	}
	
	/**
	 * Method to cheking connection to DB
	 * @return {boolean} True: connection opened; False: connection closed
	 */
	public boolean isConnection() {
		return isConnection;
	}
	
	/**
	 * Get information from table in JSON format
	 */
	public String getDealsJson(long deal_id, String from_deal_time, String to_deal_time, String deal_type,
			double from_deal_value, double to_deal_value, String instrument_name, String counterparty_name,
			String counterparty_status) throws ParseException{
		
		ArrayList<Deal> response = databaseAccess.getDeals(deal_id, from_deal_time, to_deal_time, deal_type,
				from_deal_value, to_deal_value, instrument_name, counterparty_name,
				counterparty_status);
		Gson gson = new Gson();
		String jsonDeals = gson.toJson(response);
		System.out.println(jsonDeals);
		return jsonDeals;
	}
	
	/**
	 * Get the average buy and sell prices for each instrument during the period in JSON format
	 */
	public String getAveragesJson() {
		ArrayList<AverageBuyAndSell> response = databaseAccess.getAveragesValues();
		Gson gson = new Gson();
		String jsonAverage = gson.toJson(response);
		System.out.println(jsonAverage);
		return jsonAverage;
	}
	
	/**
	 * Get dealers ending positions (i.e. how many net trades have they done on each instrument) in JSON format
	 */
	public String getEndingPositionsJson() {
		ArrayList<EndingPositions> response = databaseAccess.getEndPositions();
		Gson gson = new Gson();
		String jsonEndPositions = gson.toJson(response);
		System.out.println(jsonEndPositions);
		return jsonEndPositions;
	}
	
	/**
	 * Get the realised profit/loss for each dealer (i.e. net sum of their trade values) in JSON format
	 */
	public String getNetSumJson() {
		ArrayList<NetValues> response = databaseAccess.getNetValues();
		Gson gson = new Gson();
		String jsonNetValues = gson.toJson(response);
		System.out.println(jsonNetValues);
		return jsonNetValues;
	}
	
	/**
	 * Get the effective profit/loss for each dealer (i.e. resolving their positions using the ending buy/sell price) in JSON format
	 */
	public String getEffectiveSumJson() {
		ArrayList<NetValues> response = databaseAccess.getEffectiveSum();
		Gson gson = new Gson();
		String jsonEffectiveSumValues = gson.toJson(response);
		System.out.println(jsonEffectiveSumValues);
		return jsonEffectiveSumValues;
	}
}
