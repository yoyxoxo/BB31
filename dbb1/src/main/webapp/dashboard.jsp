<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Dashboard</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="custom.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	
	
<script>
$(document).ready(function(){
	$(document).on("click", "#Average", function() {
	//$('#Average').click(function(){
		//location.reload(true);
		$('#deals').hide();
		$('#deals_table').hide();
		$('#net').hide();
		$('#net_table').hide();
		$('#rprofit').hide();
		$("#rprofit_table").hide();
		$('#eprofit').hide();
		$("#eprofit_table").hide();
		$('#average').show();
		$("#average_table").show().html("<thead><th>Instrument</th><th>Average Buy Price</th><th>Average Sell Price</th></thead>");
		$.get("showAverage.jsp",function(data){
			var response = $.parseJSON(data);
			$(function() {
			    $.each(response, function(i, item) {
			    	//alert(item.instrument_name);
			        $('<tr>').append(
			            $('<td>').text(item.instrument_name),
			            $('<td>').text(item.average_buy_price),
			            $('<td>').text(item.average_sell_price)
			        ).appendTo('#average_table');
			    });
			});
			
		});
		
	});	
// })


// $(document).ready(function(){
	$(document).on("click", "#Net", function() {
	//$('#Net').click(function(){
		//location.reload(true);
		$('#deals').hide();
		$('#deals_table').hide();
		$('#average').hide();
		$('#averagee_table').hide();
		$('#eprofit').hide();
		$("#eprofit_table").hide();
		$('#rprofit').hide();
		$("#rprofit_table").hide();
		$('#net').show();
		$("#net_table").show().html("<thead><th>Dealers</th><th>Instruments</th><th>Net position</th></thead>");
		$.get("showNetposition.jsp",function(data){
			var response = $.parseJSON(data);
			$(function() {
			    $.each(response, function(i, item) {
			    	//alert(item.counterparty);
			        $('<tr>').append(
			            $('<td>').text(item.counterparty),
			            $('<td>').text(item.instrument),
			            $('<td>').text(item.end_positions)
			        ).appendTo('#net_table');
			    });
			});
			
		});
		
	});	
// })

// $(document).ready(function(){
	$(document).on("click", "#Rprofit", function() {
	//$('#Rprofit').click(function(){
		//location.reload(true);
		$('#deals').hide();
		$('#deals_table').hide();
		$('#average').hide();
		$('#averagee_table').hide();
		$('#net').hide();
		$("#net_table").hide();
		$('#eprofit').hide();
		$("#eprofit_table").hide();
		$('#rprofit').show();
		$("#rprofit_table").show().html("<thead><th>Dealers</th><th>Realized Profits</th></thead>");
		$.get("showRprofits.jsp",function(data){
			var response = $.parseJSON(data);
			$(function() {
			    $.each(response, function(i, item) {
			    	//alert(item.counterparty);
			        $('<tr>').append(
			            $('<td>').text(item.counterparty),
			            $('<td>').text(item.net)
			           
			        ).appendTo('#rprofit_table');
			    });
			});
			
		});
		
	});	
})



$(document).ready(function(){
	$(document).on("click", "#Eprofit", function() {
	//$('#Eprofit').click(function(){
		//location.reload(true);
		$('#deals').hide();
		$('#deals_table').hide();
		$('#average').hide();
		$('#averagee_table').hide();
		$('#net').hide();
		$("#net_table").hide();
		$('#rprofit').hide();
		$("#rprofit_table").hide();
		$('#eprofit').show()
		$("#eprofit_table").show().html("<thead><th>Dealers<th>Effective Profits</th></thead>");
		$.get("showEprofits.jsp",function(data){
			var response = $.parseJSON(data);
			$(function() {
			    $.each(response, function(i, item) {
			    	//alert(item.counterparty);
			        $('<tr>').append(
			            $('<td>').text(item.counterparty),
			            $('<td>').text(item.net)
			            
			        ).appendTo('#eprofit_table');
			    });
			});
			
		});
		
	});	
// })


// $(document).ready(function(){
	$('#Deals').click(function(){
		$('#average').hide();
		$('#averagee_table').hide();
		$('#net').hide();
		$("#net_table").hide();
		$('#rprofit').hide();
		$("#rprofit_table").hide();
		$('#deals').show();
		$('#deals_table').show();
	});	
// })
// $(document).ready(function(){
	$('#logout').click(function(){
		$('.container').load('index.jsp');
	});	
// })

// $(document).ready(function(){
	$('#filter').click(function(){
		
		var dtype= document.getElementById("deal_type").value;
		var iname= document.getElementById("inst-type").value;
		var cname= document.getElementById("counterparty").value;
		var cstatus= document.getElementById("counterstatus").value;
		//alert(iname);
		
		$("#result_table").html("<thead><th>#</th><th>Date</th><th>Instrument</th><th>Type</th><th>Counterparty</th><th>Counterparty Status</th><th>Amount</th><th>Quantity</th></thead>");
		
		$.post("showresult.jsp",{
			d_type: dtype,
			i_name: iname,
			c_name: cname,
			c_status: cstatus
		},function(data){
			var response = $.parseJSON(data);
			
			$(function() {
			    $.each(response, function(i, item) {
			    	//alert(item.deal_id);
			        $('<tr>').append(
			            $('<td>').text(item.deal_id),
			            $('<td>').text(item.deal_time),
			            $('<td>').text(item.instrument_name),
			            $('<td>').text(item.deal_type),
			            $('<td>').text(item.counterparty_name),
			            $('<td>').text(item.counterparty_status),
			            $('<td>').text(item.deal_amount),
			            $('<td>').text(item.quantity)
			        ).appendTo('#result_table');
			    });
			});
			
		});
	});
		/*var d_id = document.getElementById("d_id");
		var f_time= document.getElementById("f_time");
		var t_time= document.getElementById("t_time");
		var d_type= document.getElementById("d_type");
		var f_value= document.getElementById("");
		var t_value= document.getElementById("");
		var i_name= document.getElementById("");
		var c_name= document.getElementById("counterparty");
		var c_status= document.getElementById("counterstatus");*/
		//$.post("showresult.jsp",makeTable($("#result_table"),data));
		
});
</script>
</head>
<body>
<!-- Navigation bar -->
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <img class="navbar-brand" src="dblogo.png" /> <a
                class="navbar-brand">Deutsche Bank</a>
        </div>
 
            <button type="button" class="btn btn-default col-sm-1 pull-right pull-right-custom" id = "logout">Log Out</button>
            <button type="button" class="btn btn-default col-sm-1 pull-right pull-right-custom" id = "Eprofit">Effective</button>
            <button type="button" class="btn btn-default col-sm-1 pull-right pull-right-custom" id = "Rprofit">Realized</button>
            <button type="button" class="btn btn-default col-sm-1 pull-right pull-right-custom" id = "Net">Position</button>
            <button type="button" class="btn btn-default col-sm-1 pull-right pull-right-custom" id = "Average">Average</button>
            <button type="button" class="btn btn-default col-sm-1 pull-right pull-right-custom" id = "Deals">Deals</button>
    </div>
</nav>
<!-- End navigation bar -->
<!-- Search area -->
    <div class="panel panel-default" id = "deals">
		<div class="panel-heading panel-heading-custom">Filter Transactions</div>
		<div class="panel-body">
            <form>
                <div class="row">
                    <div class="form-group col-sm-3 form-group-custom">
                        <label class="filter-type" for="inst-type">Instrument:</label>
                        <select class="form-control form-control-custom" name="inst-type" id="inst-type">
                            <option value= "">All</option>
                            <option value="Astronomica">Astronomica</option>
                            <option value="Borealis">Borealis</option>
                            <option value="Celestial">Celestial</option>
                            <option value="Deuteronic">Deuteronic</option>
                            <option value="Eclipse">Eclipse</option>
                            <option value="Floral">Floral</option>
                            <option value="Galactica">Galactica</option>
                            <option value="Heliosphere">Heliosphere</option>
                            <option value="Interstella">Interstella</option>
                            <option value="Jupiter">Jupiter</option>
                            <option value="Koronis">Koronis</option>
                            <option value="Lunatic">Lunatic</option>
                        </select>
                    </div>
                    <div class="form-group col-sm-3 form-group-custom">
                        <label class="filter-type" for="counterparty">Counterparty:</label>
                        <input type="text" class="form-control form-control-custom" name="counterparty" id="counterparty" />
                    </div>
                    
                     <div class="form-group col-sm-3 form-group-custom">
                        <label class="filter-type" for="counterstatus">Counterparty Status::</label>
                        <select class="form-control form-control-custom" name=counterstatus id="counterstatus">
                            <option value= "">All</option>
                            <option value="A">Active</option>
                            <option value="NA">Inactive</option>
                        </select>
                    </div>
                </div>
                <div class="row">
              
              <!--   <div class="form-group col-sm-6 form-group-custom">
                        <label class="filter-type text-nowrap" for="trans-amnt">Amount (inclusive):</label>
                        <input type="text" class="form-control form-control-custom" name="min" id="trans-amnt" />&nbsp;to&nbsp;
                        <input type="text" class="form-control form-control-custom"  name="max" id="trans-amnt" />
                </div>  -->
        
                <div class="form-group col-sm-3 form-group-custom">
                        <label class="filter-type" for="deal_type">Deal Type:</label>
                        <select class="form-control form-control-custom" name="deal_type" id="deal_type">
                            <option value= "">All</option>
                            <option value="B">Buy</option>
                            <option value="S">Sell</option>
                        </select>
                    </div>
 
             
                    <button type="button" class="btn btn-default col-sm-1 pull-right pull-right-custom" id = "filter">Show Deals</button>
       
                </div>
            </form>
        </div>
    </div>
<!-- End search area -->
<!-- Results -->
<div class="panel panel-default" id="deals_table">
    <div class="panel-heading panel-heading-custom">Transactions</div>
    <div class="panel-body">
        <table class="table" id = "result_table">
            <thead>
                <th>#</th>
                <th>Date</th>
                <th>Instrument</th>
                <th>Type</th>
                <th>Counterparty</th>
                <th>Counterparty Status</th>
                <th>Amount</th>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<!-- End results -->

<div class="panel panel-default" id = "average">
<div class="panel-heading panel-heading-custom">Average Price</div>
    <div class="panel-body">
        <table class="table" id = "average_table">
            <thead>
                <th>Instrument</th>
                <th>Average Buy Price</th>
                <th>Average Sell Price</th>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

</div>

<div class="panel panel-default" id = "net">
<div class="panel-heading panel-heading-custom">Ending Position</div>
    <div class="panel-body">
        <table class="table" id = "net_table">
            <thead>
                <th>Dealer</th>
                <th>Net Position</th>
              
                
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

</div>

<div class="panel panel-default" id = "rprofit">
<div class="panel-heading panel-heading-custom">Realized Profits</div>
    <div class="panel-body">
        <table class="table" id = "rprofit_table">
            <thead>
                <th>Dealer</th>
                <th>Realized Profit</th>
             
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

</div>

<div class="panel panel-default" id = "eprofit">
<div class="panel-heading panel-heading-custom">Effective Profits</div>
    <div class="panel-body">
        <table class="table" id = "eprofit_table">
            <thead>
                <th>Dealer</th>
                
                <th>Effective Profit</th>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

</div>

</body>
</html>