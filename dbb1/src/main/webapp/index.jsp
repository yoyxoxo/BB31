<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>DBank Invest</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Welcome to DBank Invest</h2>
  <p>Please login to continue.</p>
  <form class="form-horizontal" method="post" action="LoginCheck">
    <div class="form-group">
      <label class="control-label col-sm-1" for="un">Username:</label>
      <div class="col-sm-5">
        <input type="text" class="form-control"  id = "f_userid" name="un">
    </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-1" for="pw">Password:</label>
      <div class="col-sm-5">
        <input type="password" class="form-control" id = "f_pwd" name="pw">
      </div> 
    </div>
    <button type="button" class="btn btn-default">Submit</button>
  </form>
</div>

<script>
    $(document).ready(function(){
		$('button').click(function(){
			var s1 = document.getElementById("f_userid").value;
			var s2 = document.getElementById("f_pwd").value;
			$.post("validate.jsp",{
					id: s1,
					password: s2
				}, function(data){
					alert(data);
					if(data.includes("User details are VALID"))
						$('.container').load('dashboard.jsp');
				});	
			
			});
		});
</script>

</body>
</html>