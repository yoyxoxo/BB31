package com.s2s.demomidtier;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.ArrayList;

import org.junit.Test;

import com.s2s.dbconnection.AverageBuyAndSell;
import com.s2s.dbconnection.Deal;
import com.s2s.dbconnection.EndingPositions;
import com.s2s.dbconnection.NetValues;
import com.s2s.demomidtier.Responder;

public class ResponderTest {

	private ArrayList<Deal> getDealsHelper(long deal_id, String from_deal_time, String to_deal_time, String deal_type,
			double from_deal_value, double to_deal_value, String instrument_name, String counterparty_name,
			String counterparty_status) throws ParseException {
		
		Responder responder = new Responder();
		ArrayList<Deal> deals = responder.getDeals(deal_id, from_deal_time, to_deal_time, 
				deal_type, from_deal_value, to_deal_value, 
				instrument_name, counterparty_name, counterparty_status);
		
		return deals;
	}
	
	private ArrayList<AverageBuyAndSell> getAverageHelper() {
		Responder responder = new Responder();
		ArrayList<AverageBuyAndSell> averageValues = responder.getAverages();
		return averageValues;
	}
	
	private ArrayList<EndingPositions> getEndingPositionsHelper() {
		Responder responder = new Responder();
		ArrayList<EndingPositions> endingPositions = responder.getEndPositions();	
		return endingPositions;
	}
	
	private ArrayList<NetValues> getNetValuesHelper() {	
		Responder responder = new Responder();
		ArrayList<NetValues> netValues = responder.getNetValues();
		return netValues;
	}
	
	private ArrayList<NetValues> getEffectiveSumHelper() {	
		Responder responder = new Responder();
		ArrayList<NetValues> effectiveSum = responder.getEffectiveSum();
		return effectiveSum;
	}
	
	@Test
	public void extractedDataWithoutFiltrationShouldNotBeEmpty() throws ParseException {
		ArrayList<Deal> deals = this.getDealsHelper(-1, "", "", "", -1, -1, "", "", "");
		assertNotEquals("Extracted data is empty!", deals.size(), 0);
	}
	
	@Test
	public void filteredDataByExistentDealIdShouldHaveEqualSize() throws ParseException {
		int dealId = 20011;
		ArrayList<Deal> deals = this.getDealsHelper(dealId, "", "", "", -1, -1, "", "", "");
		assertEquals("Extracted, filtered by existent deal_id = " + dealId + "data is empty!", deals.size(), 1);
	}
	
	@Test
	public void filteredDataByNonExistentDealIdShouldBeEmpty() throws ParseException {
		int dealId = -34;
		ArrayList<Deal> deals = this.getDealsHelper(dealId, "", "", "", -1, -1, "", "", "");
		assertEquals("Extracted, filtered by nonexistent deal_id = " + dealId + "data is not empty!", deals.size(), 0);
	}
	
	@Test
	public void filteredDataByMinimalFromDateShouldNotBeEmpty() throws ParseException {
		String fromDate = "2017-07-28 17:06:29.955";
		ArrayList<Deal> deals = this.getDealsHelper(-1, fromDate, "", "", -1, -1, "", "", "");
		assertNotEquals("Extracted, filtered by from deal date = " + fromDate + "data is empty!", deals.size(), 0);
	}
	
	@Test
	public void filteredDataByMaximumFromDateShouldBeEmpty() throws ParseException {
		String fromDate = "2056-07-28 17:06:29.955";
		ArrayList<Deal> deals = this.getDealsHelper(-1, fromDate, "", "", -1, -1, "", "", "");
		assertEquals("Extracted, filtered by from deal date = " + fromDate + "data is not empty!", deals.size(), 0);
	}
	
	@Test
	public void filteredDataByMinimalToDateShouldNotBeEmpty() throws ParseException {
		String toDate = "2017-25-28 17:06:29.955";
		ArrayList<Deal> deals = this.getDealsHelper(-1, "", toDate, "", -1, -1, "", "", "");
		assertNotEquals("Extracted, filtered by to deal date = " + toDate + "data is empty!", deals.size(), 0);
	}
	
	@Test
	public void filteredDataByMaximumToDateShouldBeEmpty() throws ParseException {
		String toDate = "2000-07-28 17:06:29.955";
		ArrayList<Deal> deals = this.getDealsHelper(-1, "", toDate, "", -1, -1, "", "", "");
		assertEquals("Extracted, filtered by to deal date = " + toDate + "data is not empty!", deals.size(), 0);
	}
	
	@Test
	public void filteredDataByExistentDealTypeShouldNotBeEmpty() throws ParseException {
		String dealType = "S";
		ArrayList<Deal> deals = this.getDealsHelper(-1, "", "", dealType, -1, -1, "", "", "");
		assertNotEquals("Extracted, filtered by to deal type = " + dealType + "data is empty!", deals.size(), 0);
	}
	
	@Test
	public void filteredDataByNonExistentDealTypeShouldBeEmpty() throws ParseException {
		String  dealType = "F";
		ArrayList<Deal> deals = this.getDealsHelper(-1, "", "", dealType, -1, -1, "", "", "");
		assertEquals("Extracted, filtered by to deal type = " + dealType + "data is not empty!", deals.size(), 0);
	}
	
	@Test
	public void filteredDataByMinimalFromDealValueShouldNotBeEmpty() throws ParseException {
		int fromDealValue = 0;
		ArrayList<Deal> deals = this.getDealsHelper(-1, "", "", "", fromDealValue, -1, "", "", "");
		assertNotEquals("Extracted, filtered by from deal value = " + fromDealValue + "data is empty!", deals.size(), 0);
	}
	
	@Test
	public void filteredDataByMaximumFromDealValueShouldBeEmpty() throws ParseException {
		int fromDealValue = 1000000000;
		ArrayList<Deal> deals = this.getDealsHelper(-1, "", "", "", fromDealValue, -1, "", "", "");
		assertEquals("Extracted, filtered by from deal value = " + fromDealValue + "data is not empty!", deals.size(), 0);
	}
	
	@Test
	public void filteredDataByMaximumToDealValueShouldNotBeEmpty() throws ParseException {
		int toDealValue = 100000000;
		ArrayList<Deal> deals = this.getDealsHelper(-1, "", "", "", -1, toDealValue, "", "", "");
		assertNotEquals("Extracted, filtered by to deal value = " + toDealValue + "data is not empty!", deals.size(), 0);
	}
	
	@Test
	public void filteredDataByMinimalToDealValueShouldNotBeEmpty() throws ParseException {
		int toDealValue = 0;
		ArrayList<Deal> deals = this.getDealsHelper(-1, "", "", "", -1, toDealValue, "", "", "");
		assertEquals("Extracted, filtered by to deal value = " + toDealValue + "data is empty!", deals.size(), 0);
	}
	
	@Test
	public void filteredDataByExistentInstrumentShouldNotBeEmpty() throws ParseException {
		String instrument = "Eclipse";
		ArrayList<Deal> deals = this.getDealsHelper(-1, "", "", "", -1, -1, instrument, "", "");
		assertNotEquals("Extracted, filtered by instrument = " + instrument + "data is empty!", deals.size(), 0);
	}
	
	@Test
	public void filteredDataByNonExistentInstrumentShouldBeEmpty() throws ParseException {
		String  instrument = "lalala";
		ArrayList<Deal> deals = this.getDealsHelper(-1, "", "", "", -1, -1, instrument, "", "");
		assertEquals("Extracted, filtered by instrument = " + instrument + "data is not empty!", deals.size(), 0);
	}
	
	@Test
	public void filteredDataByExistentCounterpartyNameShouldNotBeEmpty() throws ParseException {
		String counterpartyName = "Selvyn";
		ArrayList<Deal> deals = this.getDealsHelper(-1, "", "", "", -1, -1, "", counterpartyName, "");
		assertNotEquals("Extracted, filtered by counterparty name = " + counterpartyName + "data is empty!", deals.size(), 0);
	}
	
	@Test
	public void filteredDataByNonExistentCounterpartyNameShouldBeEmpty() throws ParseException {
		String  counterpartyName = "lalala";
		ArrayList<Deal> deals = this.getDealsHelper(-1, "", "", "", -1, -1, "", counterpartyName, "");
		assertEquals("Extracted, filtered by counterparty name = " + counterpartyName + "data is not empty!", deals.size(), 0);
	}
	
	@Test
	public void filteredDataByExistentCounterpartyStatusShouldNotBeEmpty() throws ParseException {
		String counterpartyStatus = "A";
		ArrayList<Deal> deals = this.getDealsHelper(-1, "", "", "", -1, -1, "", "", counterpartyStatus);
		assertNotEquals("Extracted, filtered by counterparty status = " + counterpartyStatus + "data is empty!", deals.size(), 0);
	}
	
	@Test
	public void filteredDataByNonExistentCounterpartyStatusShouldBeEmpty() throws ParseException {
		String  counterpartyStatus = "lalala";
		ArrayList<Deal> deals = this.getDealsHelper(-1, "", "", "", -1, -1, "", "", counterpartyStatus);
		assertEquals("Extracted, filtered by counterparty status = " + counterpartyStatus + "data is not empty!", deals.size(), 0);
	}
	
	@Test
	public void getAveragePriceForExistentInstrument() {
		ArrayList<AverageBuyAndSell> averages = this.getAverageHelper();
		assertNotEquals("Averages values are empty", averages.get(0).getAverage_buy_price(), 0);
	}
	
	@Test
	public void getEndPositionsOfExistentInstrument() {
		ArrayList<EndingPositions> averages = this.getEndingPositionsHelper();
		assertNotEquals("Ending positions are empty", averages.get(0).getEnd_positions(), 0);
	}
	
	@Test
	public void getNetValueForExistentCounterparty() {
		ArrayList<NetValues> netValues = this.getNetValuesHelper();
		assertNotEquals("Extracted, filtered by counterparty status = ", netValues.get(0).getNet(), 0);
	}
	
	@Test
	public void getEffectiveSumForExistentCounterparty() {
		ArrayList<NetValues> averages = this.getEffectiveSumHelper();
		assertNotEquals("Extracted, filtered by counterparty status = ", averages.get(0).getNet(), 0);
	}
}
