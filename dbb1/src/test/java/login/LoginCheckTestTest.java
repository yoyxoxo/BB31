package login;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;

import org.junit.Test;
import login.LoginCheck;

public class LoginCheckTestTest {
	
	private void isUserHelper(String username, String password, Boolean isLoginSuccessed) throws ParseException {
		LoginCheck loginCheck = new LoginCheck();
		assertEquals("User : " + username + " with password : " + password + " -> {1}", loginCheck.isUserAuthorized(username, password), isLoginSuccessed);
	}
	
	@Test
	public void setExistentUser() throws ParseException {
		isUserHelper("alison", "gradprog2016@07", true);
	}
	
	@Test
	public void setUnexistentUser() throws ParseException{
		isUserHelper("alison666", "gradprog2016@07", false);
	}
	
	@Test
	public void setUserWithEmptyLogin() throws ParseException{
		isUserHelper("", "gradprog2016@07", false);
	}
	
	@Test
	public void setUserWithEmptyPassword() throws ParseException{
		isUserHelper("alison", "", false);
	}

	@Test
	public void setUserWithEmptyLoginAndPassword() throws ParseException{
		isUserHelper("", "", false);
	}
}
